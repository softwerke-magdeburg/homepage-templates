<?php namespace ProcessWire;

/**
 * Initialize variables output in _main.php
 *
 * Values populated to these may be changed as desired by each template file.
 * You can setup as many such variables as you'd like. 
 *
 * This file is automatically prepended to all template files as a result of:
 * $config->prependTemplateFile = '_init.php'; in /site/config.php. 
 *
 * If you want to disable this automatic inclusion for any given template, 
 * go in your admin to Setup > Templates > [some-template] and click on the 
 * "Files" tab. Check the box to "Disable automatic prepend file". 
 *
 */


$content = "";

// Include shared functions (if any)
include_once("./_func.php"); 

function template_head() {
	if (!defined("HAS_HEAD")) {
		include("_head.php");
	}
}
function template_content($content) {
	if (!defined("HAS_CONTENT")) {
		define("HAS_CONTENT", true);
		echo $content;
	}
}
function template_foot() {
	if (!defined("HAS_FOOT")) {
		define("HAS_FOOT", true);
		include("_foot.php");
	}
}

// What happens after this?
// ------------------------
// 1. ProcessWire loads your page's template file (i.e. basic-page.php).
// 2. ProcessWire loads the _main.php file
// 
// See the README.txt file for more information.

