<?php namespace ProcessWire;
define("HAS_HEAD", true);
?>
<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="theme-color" content="#00ae7b">
	<title><?= title(); ?> &ndash; Softwerke Magdeburg</title>
	<meta name="description" content="<?= page()->summary; ?>" />
	<style>body { visibility: hidden; }</style>
	<link rel="stylesheet" type="text/css" href="<?= config()->urls->templates ?>styles/main.css" />
	<link rel="stylesheet" type="text/css" href="<?= config()->urls->templates ?>styles/ci-fonts.css" />
</head>
<body>

	<header>
		<h1><a href="/"><img style="transform: translateX(-0.25em); height: 3.5em;" src="<?= config()->urls->templates ?>styles/logo.svg" alt="Softwerke Magdeburg"></a></h1>
		<nav>
			<a href="/">Start</a>
			<a href="/werte/">Werte</a>
			<a href="/ueber-uns/">Über uns</a>
			<a href="/blog/">Blog</a>
			<a href="/mitmachen/">Mitmachen</a>
			<a href="/foerdern/">Fördern</a>
			<a href="/kontakt">Kontakt</a>
		</nav>
	</header>

	<main>
