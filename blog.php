<?php namespace Processwire;

$pageSize = 10;
$children = pages()->find("parent=" . page()->id . ", start=" . ((input()->pageNum - 1) * $pageSize) . ", limit=" . $pageSize . ", sort=-published");
$pageCount = ceil(count($page->children) / $pageSize);

$rssLink = page()->httpUrl() . "?rss";
if (isset(input()->get->rss)) {
	$rss = modules()->get("MarkupRSS");
	$rss->title = title();
	$rss->description = page()->summary;
	$rss->url = $rssLink;
	$rss->render($children);
	die();
}

template_head();
if (!!input()->pageNum && input()->pageNum > 1) template_content("");
else template_content(page()->body);

foreach($children as $child) { ?>

<h3 id="<?= $child->name ?>"><a href="<?= $child->url ?>"><?= $child->title ?></a></h3>
<address>
	<strong><?= $child->author ?></strong>
	<span title="<?= datetime()->date("%c", $child->published) ?>"><?= datetime()->date("relative", $child->published) ?></span>
</address>
<p>
	<?= $child->summary ?>
	<a href="<?= $child->url ?>">Weiterlesen ➜</a>
</p>

<p></p>

<?php
}

$pagePrefix = $page->url . $config->pageNumUrlPrefix;
?>
<nav class="button-group" role="navigation">
	Seiten:
	<?php if ($input->pageNum > 2) { ?><a class="button outline" href="<?= $pagePrefix ?>1" aria-label="Zu erster Seite 1">1</a><?php } ?>
	<?php if ($input->pageNum > 3) { ?><span class="button outline" disabled>&hellip;</span><?php } ?>
	<?php if ($input->pageNum > 1) { ?><a class="button outline" href="<?= $pagePrefix ?><?= input()->pageNum - 1 ?>" aria-label="Zu Seite <?= input()->pageNum - 1 ?>"><?= input()->pageNum - 1 ?></a><?php } ?>
	<a class="button" href="<?= $pagePrefix ?><?= input()->pageNum ?>" aria-label="Aktuelle Seite <?= input()->pageNum ?>"><?= input()->pageNum ?></a>
	<?php if ($input->pageNum < $pageCount - 0) { ?><a class="button outline" href="<?= $pagePrefix ?><?= input()->pageNum + 1 ?>" aria-label="Zu Seite <?= input()->pageNum + 1 ?>"><?= input()->pageNum + 1 ?></a><?php } ?>
	<?php if ($input->pageNum < $pageCount - 2) { ?><span class="button outline" disabled>&hellip;</span><?php } ?>
	<?php if ($input->pageNum < $pageCount - 1) { ?><a class="button outline" href="<?= $pagePrefix ?><?= $pageCount ?>" aria-label="Zu letzter Seite <?= $pageCount ?>"><?= $pageCount ?></a><?php } ?>
</nav>
