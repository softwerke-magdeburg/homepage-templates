setInterval(() => {
	[...document.querySelectorAll("i[class^=ri-][class*=\".svg\"]")].forEach(i => {
		const icon = "<i class=\"ri-" + i.getAttribute("class").replace(/ri-[^\/]+\/([^.]+)\.svg/, "$1") + "\"></i>";
		if (i.outerHTML != icon) i.outerHTML = icon;
	});
}, 50);
