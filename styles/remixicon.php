<?php
namespace ProcessWire {
	function remixicon(string $icon, string $alt = "") {
		return "<svg class=\"icon\" " . ($alt == "" ? "role=\"presentation\"" : "role=\"img\" aria-label=\"$alt\"") . substr(file_get_contents(dirname(__FILE__) . '/remixicon/icons/' . $icon), 4);
	}
}
