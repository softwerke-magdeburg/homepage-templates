# Softwerke: Homepage-Templates
Hier findest du die Templates für unsere auf ProcessWire basierende Vereins-Homepage.

Die eigentlichen Inhalte werden in einer Datenbank gespeichert, wenn du hierfür Vorschläge hast oder mithelfen willst, schreib uns einfach an!

Als Mitglied der AG für Öffentlichkeitsarbeit kannst du [die Admin-Oberfläche](https://softwerke.md/processwire/) nutzen, um Inhalte zu verwalten.

## Deployment
Um die Skripte auf dem Server zu aktualisieren kann folgender Befehl genutzt werden:

```
rsync -a --delete --info=progress2 --rsync-path="sudo rsync" --chown 33:82 --exclude .git ./ core@managers.swarm.softwerke.md:/var/lib/docker/volumes/homepage_site/_data/templates
```
