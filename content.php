<?php namespace Processwire;

template_head();
?>

<?php if (page()->parent->template->name == "blog") { ?>
<h2><?= page()->title ?></h2>
<address>
	<strong><?= page()->author ?></strong>
	<span title="<?= datetime()->date("%c", $child->published) ?>"><?= datetime()->date("relative", page()->published) ?></span>
	&nbsp;&ndash;&nbsp;
	<a href="<?= (strpos($_SERVER["HTTP_REFERER"], page()->parent->httpUrl) === 0 ? $_SERVER["HTTP_REFERER"] . "#" . page()->name : page()->parent->url) ?>">zurück zum Blog</a>
</address>
<?php } ?>

<div><?= page()->body ?></div>
