<?php namespace ProcessWire;
define("HAS_FOOT", true);
?>
		<!-- TODO: Block-Layout & HTML-Elemente -->
	</main>

	<footer><div>
		<div class="grid">
			<div>
				<h1>Kontakt</h1>
				<p>
					<strong>Softwerke Magdeburg&nbsp;e.&nbsp;V.</strong>
					<br style="margin-bottom: 0.4em;">
					<a title="Telefon" href="tel:+4939208899781" class="social"><?= remixicon("Device/phone-fill.svg", "Ruf uns an") ?><span>+49 39208 899781</span></a><span class="credits">(nur unregelmäßig besetzt)</span>
					<br style="margin-bottom: 0.4em;"/>
					<a title="E-Mail-Kontakt" email-link class="social"><?= remixicon("Business/mail-send-fill.svg", "Schicke uns eine E-Mail") ?><span email-link-text>kontakt at softwerke dot md</span></a>
					<a title="PGP-Schlüssel" href="https://keys.openpgp.org/vks/v1/by-fingerprint/B6EFD5F474B2C3BD1DCB026E56192401A8928E16" class="social"><?= remixicon("Business/mail-lock-fill.svg", "Schicke uns eine verschlüsselte E-Mail") ?></a>
					<a title="Mastodon" href="https://machteburch.social/@softwerke" class="social" rel="me"><?= remixicon("Logos/mastodon-fill.svg", "Mastodon") ?></a>
					<a title="Peertube" href="https://tube.tchncs.de/c/softwerke" class="social"><?= remixicon("Media/movie-fill.svg", "Peertube") ?></a>
					<a title="Matrix" href="https://matrix.to/#/#softwerke:magdeburg.jetzt" class="social"><?= remixicon("Communication/chat-private-fill.svg", "Matrix") ?></a>
					<a title="Blog via RSS" href="/blog/?rss" class="social"><?= remixicon("Device/rss-fill.svg", "RSS") ?></a>
					<!-- for slightly less spam -->
					<script>[...document.querySelectorAll("[email-link]")].forEach(el => {
						const textEl = el.querySelector("[email-link-text]") || el;
						textEl.textContent = textEl.textContent.replace(/\s+at\s+/g, "@").replace(/\s+dot\s+/g, ".");
						el.href = "mailto:" + textEl.textContent;
					});</script>
				</p>
			</div>
			<div>
				<h1>Newsletter</h1>
				<p>
					<a href="https://newsletter.softwerke.md/subscription/form"><strong>Melde dich für unseren E-Mail-Newsletter an</strong></a>
					oder <a href="https://machteburch.social/@softwerke"><strong>folge uns auf Mastodon</strong></a>, um nichts zu verpassen!
				</p>
			</div>
		</div>
		<div class="credits">
			<br class="mobile-only"><h1 class="mobile-only">Credits & Dokumente</h1><br class="mobile-only"><br class="mobile-only">
			<span>Lizenz für Inhalte & Design-Implementierung: <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de">CC BY-SA-NC 4.0</a></span><br>
			<span>Design-Inspiration: <a href="https://html5up.net/phantom">Phantom by HTML5 UP</a></span><br>
			<?php if (page()->credits) { ?><div><?= page()->credits ?></div><?php } ?>
			<br>
			<span><a href="/vereinsdokumente/"><strong>Vereinsdokumente</strong></a></span>
			<span><a href="/nutzungsbestimmungen/"><strong>Nutzungsbestimmungen</strong></a></span>
			<span><a href="/datenschutz/"><strong>Datenschutz</strong></a></span>
			<span><a href="/impressum/"><strong>Impressum</strong></a></span>
		</div>
	</div></footer>

	<script>
		window.addEventListener("load", () => {
			[...document.getElementsByClassName("lazy-background")].forEach(el => el.classList.remove("lazy-background"));
		});
	</script>
</body>
</html>
